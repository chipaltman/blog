---
layout: default.liquid

title: Thinking Before Coding
---

# I've Gotta Get My Thoughts Straight

New to Rust, new to coding; but I'm not new to thinking.
The trouble is, I feel like I'm going around in circles with a project and not making progress.

I've been working through The Rust Programming Language slowly,
watching videos, reading it and making videos,
and I feel like this is progress for me,
being someone of my own skill level and experience.

At the same time, I have the urge to envision and create something  "on my own".
Not that there's really any such thing;
one thing I've learned in my less-than-a-month of programming is how much work has been done for me.
Good ole libraries.
As a learning experience, I want to make a simple app that I will use.
Just a command-line utility;
something that lets me stash things from standard input to a file,
and can retrieve the things,
and can archive the file when it ceases to be immediately useful.

Anyway, I see that one thing I lack is a clear understanding of what I want my little project to do,
so here goes an outline:

`stash`

[with no arguments sends you to --help]

`stash Hello, World!`

* checks for an active stash file
* creates one if necessary
* prepends Hello, World! to the file's contents

`stash Something else I want to remember`

* checks for an active stash file
* creates one if necessary
* prepends Something else I want to rememeber to the file's contents

```
stash gimme

	Something else I want to rememeber
	Hello, World!
```

`stash archive`

* move / rename stash file
* there is now no active stash file

# How to Do That Stuff

One tactic I've seen Brooks Patton use is totally counter-intuitive to me,
so I want to try it.
He will sometimes start by calling functions in main that he hasn't written yet.
That's kind of like what I'm doing here;
in fact, I've seen him do _this_ too — enter commands that he imagines using as part of planning.
Is this anything like Test-Driven Development?
I guess it is.

# Pseudocode

```

fn main() {

	if input is "stash"
		* print help

	if input is "stash str"
		* check for active stash file
		if true -> add "str" to it as a new line at the top

		if false -> create a stash file and check again

	if input is "stash gimme"
		* check for active stash file
		if true -> print it to stdout

		if false -> say "there is no active stash file"

	if input is "stash archive"
		* check for active stash file
		if true -> change its status to "archived"
		archived stash files should be accessable by other means,
		but they do not appear as active
}

```

# More Thinking

A file could have an active or archived state...
I have no idea how to prepend to file contents.
The same could be accomplished with a directory and one-line files,
but that seems inefficient.

`std::fs::File` is a Struct, but I have not had any success putting it to use
How much interaction with a file is necessary?
My model assumes reading and writing from the file any time a command is given,
but it may be better to buffer the whole thing ...
But wait, as it is my conception doesn't have the app staying open,
awaiting input.
That may be harder to implement.


# Start Making Functions? Structs?
